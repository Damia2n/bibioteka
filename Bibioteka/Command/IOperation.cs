﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Command
{
    public interface IOperation
    {
        bool Execute();
    }
}
