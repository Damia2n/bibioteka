﻿namespace Bibioteka.Data.Models
{
    public enum ReservationStatus
    {
        Active,
        Cancelled,
        Finished
    }
}