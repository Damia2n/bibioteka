﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Data
{
    public class BookAuthorAndPublishingHouse
    {
        public string Author { get; set; }
        public string PublishingHouse { get; set; }

        public void Read()
        {
            Console.WriteLine("Author: " + Author);
            Console.WriteLine("Publishing House: " + PublishingHouse);
        }

        public BookAuthorAndPublishingHouse(string author, string pH)
        {
            Author = author;
            PublishingHouse = pH;
        }
    }
}
