﻿using System;

namespace Bibioteka.Data.Models
{
    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }

        public Client(int id, string name, string surname)
        {
            Id = id;
            Name = name;
            SurName = surname;
        }

        public void Read()
        {
            Console.WriteLine("Name: " + Name);
            Console.WriteLine("Surname: " + SurName);
        }
    }
}