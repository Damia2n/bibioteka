﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Data
{
    public class Book
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }

        public BookAuthorAndPublishingHouse AuthorAndPublishingHouse { get; set; }

        public Book(string id, string title, string category, BookAuthorAndPublishingHouse authorAndPublishingHouse)
        {
            Id = id;
            Title = title;
            AuthorAndPublishingHouse = authorAndPublishingHouse;
            Category = category;
        }

        public void Read()
        {
            Console.WriteLine("Id: " + Id);
            Console.WriteLine("Title: " + Title);
            Console.WriteLine("Category: " + Category);
            AuthorAndPublishingHouse.Read();
        }
    }
}
