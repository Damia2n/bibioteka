﻿using Bibioteka.Data.DataInicializer;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Data.Models
{
    public class Reservation
    {

        public string Id { get; set; }
        public Book Book { get; set; }

        public ReservationStatus Status { get; set; } = ReservationStatus.Active;

        public Client Client { get; set; }

        public Reservation(string id, Book book, Client client)
        {
            Id = id;
            Book = book;
            Client = client;
        }

        public void Read()
        {
            Console.WriteLine("Id: " + Id);
            Console.WriteLine("Book Title" + Book.Title);
            Console.WriteLine("Status " + Status);
            Client.Read();
        }
    }
}
