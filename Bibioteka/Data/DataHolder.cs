﻿using Bibioteka.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Data
{
    public static class DataHolder
    {
        public static List<Book> Books { get; set; } = new List<Book>();
        public static List<Reservation> Reservations { get; set; } = new List<Reservation>();
    }
}
