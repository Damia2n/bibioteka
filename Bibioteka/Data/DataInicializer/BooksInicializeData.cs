﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Data.DataInicializer
{
    public class BookInit
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }

        public string Author { get; set; }
        public string PublishingHouse { get; set; }
    }
    public static class BooksInicializeData
    {
        public static List<BookInit> GetBooks()
        {
            return new List<BookInit>
            {
                new BookInit
                {
                    Id = "a1",
                    Author = "Adam Mickiewicz",
                    PublishingHouse = "wydawnictwo greg",
                    Title = "Pan Tadeusz",
                    Category = "Historyczna"
                },
                 new BookInit
                {
                    Id = "a2",
                    Author = "Adam Mickiewicz",
                    PublishingHouse = "wydawnictwo greg",
                    Title = "Dziady",
                    Category = "Historyczna"
                },
                  new BookInit
                {
                    Id = "h2",
                    Author = "Henryk Sienkiewicz",
                    PublishingHouse = "wydawnictwo greg",
                    Title = "Krzyżacy",
                    Category = "Historyczna"
                },
                   new BookInit
                {
                    Id = "h1",
                    Author = "Henryk Sienkiewicz",
                    PublishingHouse = "wydawnictwo greg",
                    Title = "Potop",
                    Category = "Historyczna"
                }
            };
        }
    }
}
