﻿

using Bibioteka.Data.Factories;
using Bibioteka.Data.Models;
using System;
using System.Linq;

namespace Bibioteka.Data.DataInicializer
{
    public class DataInicialize
    {
        public void InitData()
        {
            InitBooks();
            InitReservations();
        }

        private void InitReservations()
        {
            var client = ReservationClientFactory.GetClient("Damian", "Kontek");

            DataHolder.Reservations.Add(new Reservation("dk1", DataHolder.Books.FirstOrDefault(), client));
        }

        private void InitBooks()
        {
            foreach(var book in BooksInicializeData.GetBooks())
            {
                var authorAndPH = BookAuthorAndPublishingHouseFactory.GetAuthorAndPublishingHouse(book.Author, book.PublishingHouse);

                DataHolder.Books.Add(new Book(book.Id,book.Title, book.Category, authorAndPH));
            }
        }
    }
}
