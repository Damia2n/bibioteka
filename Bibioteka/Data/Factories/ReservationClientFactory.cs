﻿using Bibioteka.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bibioteka.Data.Factories
{
    public class ReservationClientFactory
    {
        static List<Client> Clients = new List<Client>();

        public static Client GetClient (string name, string surname)
        {
            var client = Clients.FirstOrDefault(x => x.Name == name && x.SurName == surname);
            if (client == null)
            {
                client = new Client(Clients.Count + 1,name, surname);
                Clients.Add(client);
            }

            return client;
        }
    }
}
