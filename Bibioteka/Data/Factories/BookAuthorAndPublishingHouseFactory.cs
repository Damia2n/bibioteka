﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bibioteka.Data
{
    public class BookAuthorAndPublishingHouseFactory
    {
        static List<BookAuthorAndPublishingHouse> bookAuthorAndPublishingHouse = new List<BookAuthorAndPublishingHouse>();

        public static BookAuthorAndPublishingHouse GetAuthorAndPublishingHouse(string author, string pH)
        {
            var authorAndPH = bookAuthorAndPublishingHouse.FirstOrDefault(x => x.Author == author && x.PublishingHouse == pH);
            if (authorAndPH == null)
            {
                authorAndPH = new BookAuthorAndPublishingHouse(author, pH);
                bookAuthorAndPublishingHouse.Add(authorAndPH);
            }   

            return authorAndPH;
        }

    }
}
