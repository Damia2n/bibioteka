﻿using Bibioteka.Command;
using Bibioteka.Library;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Books
{
    public class ShowBooksOperation : IOperation
    {
        BooksExecuter executer;
        ILibrary _app;

        public ShowBooksOperation(ILibrary app)
        {
            _app = app;
            executer = new BooksExecuter(_app);
        }

        public bool Execute()
        {
            return executer.ShowBooks();
        }
    }
}
