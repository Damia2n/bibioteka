﻿using Bibioteka.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Books
{
    public class BookState
    {
        private IBookReservationState reservationState;
        public Book _book;

        public BookState(bool haveReservation, Book book)
        {
            if (haveReservation)
            {
                reservationState = new Reservet();
            }
            else
            {
                reservationState = new NotResevet();
            }
            reservationState.ChangeState(this);
            _book = book;
        }

        public void ChangeState(IBookReservationState state)
        {
            reservationState = state;
            reservationState.ChangeState(this);
            Read();
        }

        public void Read()
        {
            Console.Clear();
            _book.Read();
            reservationState.ReadMenu();
        }
    }
}
