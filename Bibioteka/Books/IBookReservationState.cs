﻿namespace Bibioteka.Books
{
    public interface IBookReservationState
    {
        void ChangeState(BookState bookSate);
        void ReadMenu();
    }
}