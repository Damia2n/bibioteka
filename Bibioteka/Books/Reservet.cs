﻿using Bibioteka.Data;
using Bibioteka.Data.Models;
using System;
using System.Linq;

namespace Bibioteka.Books
{
    public class Reservet : IBookReservationState
    {
        private BookState _bookState;
        public void ChangeState(BookState bookState)
        {
            _bookState = bookState;
        }

        public void ReadMenu()
        {
            Console.WriteLine("Menu");
            Console.WriteLine("1. UnReserve");
            Console.WriteLine("0. Back");

            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.D1:
                        var resarvation = DataHolder.Reservations.FirstOrDefault(x=>x.Book.Id == _bookState._book.Id);
                        resarvation.Status = ReservationStatus.Cancelled;
                        _bookState.ChangeState(new NotResevet());
                        break;
                }
            }
            while (key.Key != ConsoleKey.D0);
        }
    }
}