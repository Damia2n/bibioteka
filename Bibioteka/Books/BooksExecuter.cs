﻿using Bibioteka.Data;
using Bibioteka.Data.Models;
using Bibioteka.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bibioteka.Books
{
    public class BooksExecuter
    {
        private readonly ILibrary _app;

        public BooksExecuter(ILibrary app)
        {
            _app = app;
        }

        public bool ShowBooks()
        {
            foreach (var book in DataHolder.Books)
            {
                book.Read();
                Console.WriteLine("============");
            }

            if (nameof(AdminLibrary) == _app.GetType().Name)
            {
                ShowAdminMenu();
            }
            else
            {
                ShowMenu();
            }


            return true;
        }

        private void ShowAdminMenu()
        {
            Console.WriteLine("Menu");
            Console.WriteLine("1. GoToDetials");
            Console.WriteLine("2. Edit");
            Console.WriteLine("3. AddNew");
            Console.WriteLine("4. Delete");
            Console.WriteLine("0. Back");

            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.D1:
                        var book = FindBook();
                        if(book != null)
                        {
                            book.Read();
                            Console.WriteLine("Press any button to back");
                            Console.ReadKey();
                        }
                        break;
                    case ConsoleKey.D2:
                        book = FindBook();
                        if (book != null)
                        {
                            Console.Write("Id: ");
                            book.Id = Console.ReadLine();
                            Console.Write("Title: ");
                            book.Title = Console.ReadLine();
                            Console.Write("Category: ");
                            book.Category = Console.ReadLine();
                            Console.Write("Author: ");
                            var author = Console.ReadLine();
                            Console.Write("Publishing house: ");
                            var ph = Console.ReadLine();
                            book.AuthorAndPublishingHouse = BookAuthorAndPublishingHouseFactory.GetAuthorAndPublishingHouse(author, ph);
                        }
                        break;
                    case ConsoleKey.D3:
                        Console.Write("Id: ");
                        var Id = Console.ReadLine();
                        Console.Write("Title: ");
                        var Title = Console.ReadLine();
                        Console.Write("Category: ");
                        var Category = Console.ReadLine();
                        Console.Write("Author: ");
                        var autho = Console.ReadLine();
                        Console.Write("Publishing house: ");
                        var publishingHourse = Console.ReadLine();
                        var authorAndPH = BookAuthorAndPublishingHouseFactory.GetAuthorAndPublishingHouse(autho, publishingHourse);
                        DataHolder.Books.Add(new Book(Id, Title, Category, authorAndPH));
                        break;
                    case ConsoleKey.D4:
                        book = FindBook();
                        if (book != null)
                        {
                            DataHolder.Books.Remove(book);
                        }
                        break;
                }
                if(key.Key != ConsoleKey.D0)
                {
                    Console.Clear();
                    ShowBooks();
                }
            }
            while (key.Key != ConsoleKey.D0);
        }

        private void ShowMenu()
        {
            var book = FindBook();

            if (book != null)
            {
                var bookState = new BookState(DataHolder.Reservations.Any(x => x.Book.Id == book.Id && x.Client.Id == 1), book);
                bookState.Read();
                Console.Clear();
                ShowBooks();
            }
        }

        private Book FindBook()
        {
            Console.WriteLine("Type Id and press enter or press esc to back");
            string result = null;

            StringBuilder buffer = new StringBuilder();

            ConsoleKeyInfo info = Console.ReadKey(true);
            while (info.Key != ConsoleKey.Enter && info.Key != ConsoleKey.Escape)
            {
                Console.Write(info.KeyChar);
                buffer.Append(info.KeyChar);
                info = Console.ReadKey(true);
            }

            if (info.Key == ConsoleKey.Enter)
            {
                result = buffer.ToString();
                var book = DataHolder.Books.FirstOrDefault(x => x.Id == result);

                if (book != null)
                {
                    return book;
                }
                else
                {
                    Console.WriteLine("Book with this id not find");
                    return FindBook();
                }
            }

            return null;
        }
    }
}
