﻿using Bibioteka.Data;
using Bibioteka.Data.Factories;
using Bibioteka.Data.Models;
using System;
using System.Linq;

namespace Bibioteka.Books
{
    public class NotResevet : IBookReservationState
    {
        private BookState _bookState;
        public void ChangeState(BookState bookState)
        {
            _bookState = bookState;
        }

        public void ReadMenu()
        {
            Console.WriteLine("Menu");
            Console.WriteLine("1. Reserve");
            Console.WriteLine("0. Back");

            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.D1:
                        var client = ReservationClientFactory.GetClient("Damian", "Kontek");
                        DataHolder.Reservations.Add(new Reservation("dk" + (DataHolder.Reservations.Count(x => x.Client == client)+1), _bookState._book, client));
                        _bookState.ChangeState(new Reservet());
                        break;
                }
            }
            while (key.Key != ConsoleKey.D0);
        }
    }
}