﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Library
{
    public class AdminApp : IAppCreator
    {
        public ILibrary GetLibraryApp()
        {
            return new AdminLibrary();
        }
    }
}
