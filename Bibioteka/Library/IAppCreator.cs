﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Library
{
    public interface IAppCreator
    {
        ILibrary GetLibraryApp();
    }
}
