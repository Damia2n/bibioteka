﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Library
{
    public class ClientApp : IAppCreator
    {
        public ILibrary GetLibraryApp()
        {
            return new ClientLibrary();
        }
    }
}
