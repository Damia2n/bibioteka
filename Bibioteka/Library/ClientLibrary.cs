﻿using Bibioteka.Books;
using Bibioteka.Data;
using Bibioteka.Reservations;
using Bibioteka.Reservations.Bibioteka.Reservations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Library
{
    public class ClientLibrary : ILibrary
    {
        public void ShowAppMenu()
        {
            ShowMenu();
             ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.D1:
                        Console.Clear();
                        var result = new ShowBooksOperation(this).Execute();
                        if (result)
                            ShowMenu();
                        break;
                    case ConsoleKey.D2:
                        Console.Clear();
                        result = new ShowMyReservationsOperation().Execute();
                        if (result)
                            ShowMenu();
                        break;
                    default:
                    case ConsoleKey.D0:
                        break;
                }
            }
            while (key.Key != ConsoleKey.D0);
          
        }

        private void ShowMenu()
        {
            Console.Clear();
            Console.WriteLine("Menu");
            Console.WriteLine("1. Books");
            Console.WriteLine("2. My Reservatons");
            Console.WriteLine("0. Close");
        }
    }
}
