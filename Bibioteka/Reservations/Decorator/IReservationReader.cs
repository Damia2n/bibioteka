﻿using Bibioteka.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Reservations
{
    public interface IReservationReader
    {
        void ReadReservation(List<Reservation> reservations, bool showReservedBy = false);
    }
}
