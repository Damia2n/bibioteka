﻿using Bibioteka.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bibioteka.Reservations
{
    public class FinishedReservationDecoder: IReservationReader
    {
        private readonly IReservationReader _reader;

        public FinishedReservationDecoder(IReservationReader reader)
        {
            _reader = reader;
        }

        public void ReadReservation(List<Reservation> reservations, bool showReservedBy = false)
        {
            _reader.ReadReservation(reservations.Where(x => x.Status == ReservationStatus.Finished).ToList(), showReservedBy);
        }
    }
}
