﻿using Bibioteka.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bibioteka.Reservations
{
    public class ReservationReader : IReservationReader
    {
        public void ReadReservation(List<Reservation> reservations, bool showReservedBy = false)
        {
            if (reservations.Any())
            {
                foreach (var resevation in reservations)
                {
                    Console.WriteLine("Reservation Id " + resevation.Id);
                    Console.WriteLine("Book Title " + resevation.Book.Title);
                    if (showReservedBy)
                    {
                        Console.WriteLine("Status " + resevation.Status);
                        Console.WriteLine("Reserve by ");
                        resevation.Client.Read();
                    }
                    Console.WriteLine("============");
                }
            }
            else
            {
                Console.WriteLine("No reservation");
            }
        }
    }
}
