﻿using Bibioteka.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bibioteka.Reservations
{
    public class ActiveReservationDecorator : IReservationReader
    {
        private readonly IReservationReader _reader;

        public ActiveReservationDecorator(IReservationReader reader)
        {
            _reader = reader;
        }

        public void ReadReservation(List<Reservation> reservations, bool showReservedBy = false)
        {
            _reader.ReadReservation(reservations.Where(x=>x.Status == ReservationStatus.Active).ToList(), showReservedBy);
        }
    }
}
