﻿using Bibioteka.Data;
using Bibioteka.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bibioteka.Reservations
{
    public class ReservationsExecuter
    {
        public bool ShowMyReservations()
        {
            new ActiveReservationDecorator(new ReservationReader()).ReadReservation(DataHolder.Reservations.Where(x => x.Client.Id == 1).ToList());

            Console.WriteLine("Type reservation Id and press enter to finish reservation or press esc to back");
            ShowMenu();

            return true;
        }

        public bool ShowReservations()
        {
            new ReservationReader().ReadReservation(DataHolder.Reservations);

            Console.WriteLine("Type reservation Id and press enter to show details or press esc to back");
            ShowAdminMenu();

            return true;
        }

        public bool ShowActiveReservations()
        {
            new ActiveReservationDecorator(new ReservationReader()).ReadReservation(DataHolder.Reservations);

            Console.WriteLine("Type reservation Id and press enter to show details or press esc to back");
            ShowAdminMenuForActiveReservations();

            return true;
        }

        private void ShowAdminMenu()
        {
            var reservation = FindReservation();

            if (reservation != null)
            {
                var reservationDetails = new ReservationDetails(reservation);
                reservationDetails.Read();

                Console.Clear();
                ShowReservations();
            }
        }

        private void ShowAdminMenuForActiveReservations()
        {
            var reservation = FindReservation();

            if (reservation != null)
            {
                var reservationDetails = new ReservationDetails(reservation);
                reservationDetails.Read();

                Console.Clear();
                ShowActiveReservations();
            }
        }

        private void ShowMenu()
        {
            var reservation = FindReservation();

            if (reservation != null)
            {
                reservation.Status = ReservationStatus.Finished;
                Console.Clear();
                ShowMyReservations();
            }
        }

        public Reservation FindReservation()
        {
            string result = null;

            StringBuilder buffer = new StringBuilder();

            ConsoleKeyInfo info = Console.ReadKey(true);
            while (info.Key != ConsoleKey.Enter && info.Key != ConsoleKey.Escape)
            {
                Console.Write(info.KeyChar);
                buffer.Append(info.KeyChar);
                info = Console.ReadKey(true);
            }

            if (info.Key == ConsoleKey.Enter)
            {
                result = buffer.ToString();

                var reservation = DataHolder.Reservations.FirstOrDefault(x => x.Id == result);
                if (reservation != null)
                {
                    return reservation;
                }
                else
                {
                    Console.WriteLine("Reservation with this id not find");
                    return FindReservation();
                }
            }

            return null;
        }
    }
}
