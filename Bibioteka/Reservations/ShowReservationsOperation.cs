﻿using Bibioteka.Command;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Reservations
{

    namespace Bibioteka.Reservations
    {
        public class ShowReservationsOperation : IOperation
        {
            private ReservationsExecuter Executer = new ReservationsExecuter();
            public bool Execute()
            {
                return Executer.ShowReservations();
            }
        }
    }

}
