﻿using Bibioteka.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Reservations.State
{
    public class FinishedReservationState : IReservationState
    {
        private ReservationDetails _reservationDetails;
        public void ChangeStatus(ReservationDetails reservationDetails)
        {
            _reservationDetails = reservationDetails;
        }

        public void ReadMenu()
        {
            Console.WriteLine("Menu");
            Console.WriteLine("1. Active");
            Console.WriteLine("2. Cancel");
            Console.WriteLine("0. Back");

            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.D1:
                        _reservationDetails._reservation.Status = ReservationStatus.Active;
                        _reservationDetails.ChangeState(new ActiveReservationState());
                        break;

                    case ConsoleKey.D2:
                        _reservationDetails._reservation.Status = ReservationStatus.Cancelled;
                        _reservationDetails.ChangeState(new CancelledReservationState());
                        break;
                }
            }
            while (key.Key != ConsoleKey.D0);
        }
    }
}
