﻿using Bibioteka.Data.Models;
using Bibioteka.Reservations.State;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Reservations
{
    public class ActiveReservationState : IReservationState
    {
        private ReservationDetails _reservationDetails;
        public void ChangeStatus(ReservationDetails reservationDetails)
        {
            _reservationDetails = reservationDetails;
        }

        public void ReadMenu()
        {
            Console.WriteLine("Menu");
            Console.WriteLine("1. Cancel");
            Console.WriteLine("2. Finish");
            Console.WriteLine("0. Back");

            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.D1:
                        _reservationDetails._reservation.Status = ReservationStatus.Cancelled;
                        _reservationDetails.ChangeState(new CancelledReservationState());
                        break;

                    case ConsoleKey.D2:
                        _reservationDetails._reservation.Status = ReservationStatus.Finished;
                        _reservationDetails.ChangeState(new FinishedReservationState());
                        break;
                }
            }
            while (key.Key != ConsoleKey.D0);
        }
    }
}
