﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Reservations
{
    public interface IReservationState
    {
        void ChangeStatus(ReservationDetails reservationDetails);
        void ReadMenu();
    }
}
