﻿using Bibioteka.Data.Models;
using Bibioteka.Reservations.State;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Reservations
{
    public class ReservationDetails
    {
        public Reservation _reservation;
        private IReservationState reservationState;

        public ReservationDetails(Reservation reservation)
        {
            switch (reservation.Status)
            {
                case ReservationStatus.Active:
                    reservationState = new ActiveReservationState();
                    break;
                case ReservationStatus.Cancelled:
                    reservationState = new CancelledReservationState();
                    break;
                case ReservationStatus.Finished:
                    reservationState = new FinishedReservationState();
                    break;
            }

            reservationState.ChangeStatus(this);
            _reservation = reservation;
        }

        public void ChangeState(IReservationState state)
        {
            reservationState = state;
            reservationState.ChangeStatus(this);
            Read();
        }

        public void Read()
        {
            Console.Clear();
            _reservation.Read();
            reservationState.ReadMenu();
        }
    }
}
