﻿using Bibioteka.Command;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bibioteka.Reservations
{
    public class ShowActiveReservationsOperations : IOperation
    {
        private ReservationsExecuter Executer = new ReservationsExecuter();
        public bool Execute()
        {
            return Executer.ShowActiveReservations();
        }
    }
}
