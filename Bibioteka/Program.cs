﻿using Bibioteka.Data;
using Bibioteka.Data.DataInicializer;
using Bibioteka.Library;
using System;

namespace Bibioteka
{
    class Program
    {
        static void Main(string[] args)
        {
            new DataInicialize().InitData();

            ConsoleKeyInfo key;
            do
            {
                Console.Clear();
                Console.WriteLine("Select App Type");
                Console.WriteLine("1. User App");
                Console.WriteLine("2. Admin App");
                Console.WriteLine("0. Close");


                ILibrary app = null;

                key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.D1:
                        app = new ClientApp().GetLibraryApp();
                        break;
                    case ConsoleKey.D2:
                        app = new AdminApp().GetLibraryApp();
                        break;
                    default:
                    case ConsoleKey.D0:
                        break;
                }

                if (app != null)
                {
                    Console.Clear();
                    app.ShowAppMenu();
                }
            }
            while (key.Key != ConsoleKey.D0);
        }
    }
}
